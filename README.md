# ONLYOFFICE DOCKER

Déploiement de Onlyoffice via docker compose pour le connecter à un nextcloud par exemple

![[Site Officiel](https://www.onlyoffice.com/fr)](docs/onlyoffice_logo.png)

## PRE REQUIS :paperclip:

- Docker :whale:

## CONFIGURATION :wrench:

### NGINX REVERSE PROXY

```conf
upstream onlyoffice {
    server 127.0.0.1:${HOST_PORT};
}


server {
   listen 80;
   listen [::]:80;
   server_name onlyoffice.legaragenumerique.fr;

    if ($host = onlyoffice.legaragenumerique.fr) {
        return 301 https://$host$request_uri;
    }
}

server{
   listen 443 ssl;
   listen [::]:443 ssl;
   server_name onlyoffice.legaragenumerique.fr;

   ssl on;
   ssl_certificate /etc/letsencrypt/live/onlyoffice.legaragenumerique.fr/fullchain.pem;
   ssl_certificate_key /etc/letsencrypt/live/onlyoffice.legaragenumerique.fr/privkey.pem;

   location / {
      proxy_redirect off;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Host $server_name;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_pass http://onlyoffice;
   }
}
```

:warning: Attention à renseigner le même HOST_PORT dans le bloc upstream que dans le .env 

## UTILISATION :rocket:

```bash
docker-comose up -d
```

### NEXTLOUD LINK :link:

- get secret key:
```bash
docker exec onlyoffice /var/www/onlyoffice/documentserver/npm/json -f /etc/onlyoffice/documentserver/local.json 'services.CoAuthoring.secret.session.string'
```

- ajouter l'adresse de onlyoffice + la clef secrete

![img1](docs/img1.png)

## DOCUMENTATION :books:

[Documentation Docker](https://helpcenter.onlyoffice.com/fr/installation/docs-community-docker-compose.aspx)

[Documentation Onlyoffice](https://helpcenter.onlyoffice.com/fr/installation/docs-index.aspx)

## TO DO 

- [X] vars dans .env
- [X] rabbitmq ? -> shooté
- [X] nginx reverse conf
- [X] really need volumes ?! -> NON!

